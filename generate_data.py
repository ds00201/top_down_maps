import requests
import os
import glob
from PIL import Image
from io import BytesIO
import random
import shutil
import pandas as pd


# --- SET UP ---

# API key for accessing Google Maps Static API
API_KEY = "your_api_key"                    # for security purposes you should hide your exact key when sharing code

# Path to CSV file containing latitude and longitude of EV charger locations
path_to_csv = 'ncr_filtered_lat_long.csv'   # your path may differ
df = pd.read_csv(path_to_csv)               # create a Pandas dataframe of the CSV data

# Create directories to store map tiles for EV charger placement areas and no EV charger placement areas
folder = 'map_tiles'                        # your path may differ
os.makedirs(f'{folder}/ev_charger', exist_ok=True)
os.makedirs(f'{folder}/no_ev_charger', exist_ok=True)


# --- FUNCTION DEFINITIONS ---

# Function to generate random latitude and longitude coordinates within a specified range
def generate_random_coordinates(num_coords, lat_range, lon_range):
    coordinates = []
    for _ in range(num_coords):
        lat = random.uniform(lat_range[0], lat_range[1])
        lon = random.uniform(lon_range[0], lon_range[1])
        coordinates.append((lat, lon))
    return coordinates


# Function to create and save map tiles for given coordinates
def create_map_tile(lat, lon, label, zoom=18, size=(640, 640), scale=4, folder=f'{folder}', map_type='satellite'):
    # Construct the URL for the Google Maps Static API request
    url = f"https://maps.googleapis.com/maps/api/staticmap?center={lat},{lon}&zoom={zoom}&size={size[0]}x{size[1]}&maptype={map_type}&scale={scale}&key={API_KEY}"

    # Send a request to Google Maps Static API and handle the response
    response = requests.get(url)
    print(f"API request status code (200 is good): {response.status_code}")

    # If the request is successful, save the map image
    if response.status_code == 200:
        img = Image.open(BytesIO(response.content))

        # Create a unique filename for the map tile
        base_filename = os.path.join(folder, f'{label}_map_tile_{lat}_{lon}.png')
        filename = base_filename
        counter = 2                         # for creating duplicates if a file with the same name already exists

        # Ensure filename is unique by appending a counter if a file with the same name already exists
        while os.path.exists(filename):
            filename = os.path.join(folder, f'{label}_map_tile_{lat}_{lon}({counter}).png')
            counter += 1

        # Save the map tile to the specified folder
        img.save(filename)
        print(f"Map tile saved to {filename}")
    else:
        print(f"Failed to retrieve map tile for {lat}, {lon}")


# --- CREATING THE DATA ---

# Define the number of positive map tiles (EV charger locations) desired
pos_maps_desired = 42056

# Check how many map tiles already exist and how many more are needed
# This helps to resume data collection if it is halted for some reason
existing_pos_maps = len(glob.glob(os.path.join(f'{folder}/ev_charger', '*.[jp][pn]g')))
remaining_pos_maps = pos_maps_desired - existing_pos_maps
print(f"\nThere are {existing_pos_maps} positive maps.\n"
      f"{remaining_pos_maps} more positive maps need to be extracted.\n")

# Create map tiles for each EV charger location
for idx, row in df.iloc[existing_pos_maps:existing_pos_maps + remaining_pos_maps].iterrows():
    create_map_tile(row['latitude'], row['longitude'], label=1, folder=f'{folder}/ev_charger')
# Update the number of existing positive maps in the folder, to help with generation of negative maps
existing_pos_maps = len(glob.glob(os.path.join(f'{folder}/ev_charger', '*.[jp][pn]g')))

# Check how many negative map tiles (no EV charger) already exist and how many more are needed
# To create a balanced dataset, the number of negative maps equals the number of positive maps
existing_neg_maps = len(glob.glob(os.path.join(f'{folder}/no_ev_charger', '*.[jp][pn]g')))
remaining_neg_maps = existing_pos_maps - existing_neg_maps
lat_range = (50.9, 54.2)  # Refined latitude range for the UK
lon_range = (-2.69, -0.226)  # Refined longitude range for the UK
print(f"There are {existing_neg_maps} negative maps.\n"
      f"{remaining_neg_maps} more negative maps need to be extracted.\n")

# Generate random coordinates for the negative samples (no EV charger areas)
random_coords = generate_random_coordinates(remaining_neg_maps, lat_range, lon_range)

# Create map tiles for random locations without EV chargers
for lat, lon in random_coords:
    create_map_tile(lat, lon, label=0, folder=f'{folder}/no_ev_charger')
# Update the number of existing negative maps in the folder, to be printed at the end to verify the correct amount
existing_neg_maps = len(glob.glob(os.path.join(f'{folder}/no_ev_charger', '*.[jp][pn]g')))


# --- COMBINING DATA INTO A SINGLE FOLDER ---

# Function to combine map tiles from EV charger and no EV charger folders into a single dataset
def create_combined_dataset(ev_charger_folder, no_ev_charger_folder, output_folder):
    # If the combined dataset folder already exists, skip execution of this function
    if os.path.exists(output_folder):
        print("Combined dataset already exists. Verify that it contains all the map tiles. If not, then delete the"
              "'combined_dataset' folder and run this script again.")
        pass
    else:
        os.makedirs(output_folder)

    # Get a list of all the files in the ev charger and no ev charger directories
    ev_charger_files = os.listdir(ev_charger_folder)
    no_ev_charger_files = os.listdir(no_ev_charger_folder)

    # Initialize list to store file paths for the combined dataset
    labeled_dataset = []

    # Copy EV charger files to the output folder and add to the dataset
    for file in ev_charger_files:
        src = os.path.join(ev_charger_folder, file)
        dst = os.path.join(output_folder, file)
        shutil.copy(src, dst)
        labeled_dataset.append(dst)

    # Copy no EV charger files to the output folder and add to the dataset
    for file in no_ev_charger_files:
        src = os.path.join(no_ev_charger_folder, file)
        dst = os.path.join(output_folder, file)
        shutil.copy(src, dst)
        labeled_dataset.append(dst)

    return labeled_dataset


# Create a combined labeled dataset with both EV charger and no EV charger images
combined_dataset = create_combined_dataset(f'{folder}/ev_charger', f'{folder}/no_ev_charger', f'{folder}/combined_dataset')

# Print the number of positive and negative map tiles generated, to verify they are the correct amounts
print(f"\nThere are {existing_pos_maps} number of positive maps."
      f"\nThere are {existing_neg_maps} number of negative maps.")
