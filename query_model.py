import torch
import torch.nn.functional as F
from torchvision import transforms
from PIL import Image
import os
import random
import requests
from io import BytesIO


# API key for accessing Google Maps Static API
API_KEY = "your_api_key"                    # for security purposes you should hide your exact key when sharing code

# Number of random queries for classification
number_of_queries = 100

# Define transformation for the input images
# These transformations match the ones used during model training to ensure consistency
transform = transforms.Compose([
    transforms.Resize((224, 224)),          # resize image to 224x224 pixels
    transforms.RandomHorizontalFlip(p=0.3), # random horizontal flip with 30% probability
    transforms.RandomVerticalFlip(p=0.3),   # random vertical flip with 30% probability
    transforms.RandomRotation(10),          # random rotation by up to 10 degrees
    transforms.ToTensor(),                  # convert image to PyTorch tensor
])


# Load the saved MobileNetV3 model
class FineTuneMobileNetV3(torch.nn.Module):
    def __init__(self):
        super(FineTuneMobileNetV3, self).__init__()
        # Load the MobileNetV3 model with pre-trained weights
        self.mobilenet = torch.hub.load('pytorch/vision:v0.10.0', 'mobilenet_v3_large', pretrained=True)
        # Modify the classifier to predict 2 classes (EV charger or no EV charger)
        self.mobilenet.classifier[3] = torch.nn.Linear(self.mobilenet.classifier[3].in_features, 2)
        self.name = 'MobileNetV3'

    def forward(self, x):
        return self.mobilenet(x)


# Initialize and load the MobileNetV3 model
model = FineTuneMobileNetV3()

# Load the pre-trained model weights
# Use the appropriate line for GPU or CPU (comment/uncomment as needed)
# model.load_state_dict(torch.load("MobileNetV3_50e_32bs.pth"))                                 # for GPU
model.load_state_dict(torch.load("MobileNetV3_50e_32bs.pth", map_location=torch.device('cpu'))) # for CPU

# Set the model to evaluation mode (no gradient updates, inference only)
model.eval()


# --- HELPER FUNCTIONS ---

# Function to generate random coordinates within the UK
def get_random_uk_coordinates():
    lat = random.uniform(50.9, 54.2)        # refined UK latitude range
    lon = random.uniform(-2.69, -0.226)     # refined UK longitude range
    return lat, lon


# Function to fetch a satellite image from the Google Maps API based on latitude and longitude
def get_satellite_image(lat, lon):
    # API URL to fetch satellite image from Google Maps Static API
    url = f"https://maps.googleapis.com/maps/api/staticmap?center={lat},{lon}&zoom=18&size=640x640&maptype=satellite&key={API_KEY}"
    response = requests.get(url)            # make the request to Google Maps
    img = Image.open(BytesIO(response.content)).convert('RGB')              # convert response to RGB image
    return img


# Create directories for saving classified images
# Directory '1' for EV charger predictions, '0' for no EV charger
os.makedirs('1', exist_ok=True)
os.makedirs('0', exist_ok=True)


# Function to classify the image and save it to the appropriate folder
def classify_and_save_image(img, coord, index):
    # Apply transformations to the image and add a batch dimension (unsqueeze(0) adds a dimension)
    image = transform(img).unsqueeze(0)
    # Pass the transformed image through the model to get predictions
    output = model(image)
    # Apply softmax to get probabilities for each class
    probs = F.softmax(output, dim=1)
    # Get the predicted class (0 = no EV charger, 1 = EV charger)
    prediction = torch.argmax(probs, dim=1).item()

    # Determine the folder (0 or 1) based on the prediction
    folder = str(prediction)
    # Generate a filename that includes the coordinates and index for uniqueness
    filename = f"{folder}/image_{index}_lat{coord[0]}_lon{coord[1]}.png"
    # Save the image in the corresponding folder
    img.save(filename)
    # Print out the saved image's location and the prediction
    print(f"Image saved to {filename} (Prediction: {prediction})")


# Main function to fetch and classify random UK satellite images
def main():
    for i in range(number_of_queries):
        # Get random UK coordinates
        lat, lon = get_random_uk_coordinates()
        print(f"\nFetching satellite image for coordinates: ({lat:.2f}, {lon:.2f})")
        # Fetch the satellite image from Google Maps
        img = get_satellite_image(lat, lon)
        # Classify the image and save it
        classify_and_save_image(img, (lat, lon), i)


# Run the main function if this script is executed
if __name__ == "__main__":
    main()
