**Code that I have created for my project "Predicting Electric Vehicle Charger Locations Using Neural Networks"**

clean_csv.py is a Python script that cleans the raw Nation Charge Registry CSV file.

draw_bounding_box.py is a Python script that draws a bounding box on an image to visualise parts of the YOLO algorithm.

generate_data.py is a Python script that harvests satellite map data from Google Static Maps API, creates appropriate folders, labels data, and saves data to the appropriate locations.

nn.py is a Python script handling all the data splitting and normalisation, neural network architecutre creation, neural network training, validation, and testing, and collection and printing of results.

query_model.py is a Python script that queries the trained model to simulate an example of a practical use case for the model, to show how it can aid human experts in predicting EV charger locations.

yolo_file_prep.py is a Python script that takes the harvested data and organises it into a different file strcuture, while also adding corresponding .txt files for each map. YOLO algorithm requires a unique structure of data and the purpose of this script is to get the data in the correct structure.

trained_mnv3_50_epochs.pth is the final proposed model created from my research.

Note that to use the model, image data must be first transformed using the same transforms that the neural network was trained on, namely:

Define transformations for the images:

transform = transforms.Compose([
    transforms.Resize((224, 224)),              # resize all images to 224x224
    transforms.RandomHorizontalFlip(p=0.3),     # apply horizontal flip with 30% probability
    transforms.RandomVerticalFlip(p=0.3),       # apply vertical flip with 30% probability
    transforms.RandomRotation(10),              # apply random rotation of up to 10 degrees
    transforms.ToTensor(),                      # convert the image to a PyTorch tensor
])
