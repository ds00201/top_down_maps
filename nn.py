import os
import torch.utils.data
from PIL import Image
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
from torchvision import models
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim.lr_scheduler as lr_scheduler
from sklearn.metrics import classification_report, roc_curve, roc_auc_score
import matplotlib.pyplot as plt
import time


# --- SETTING UP THE DATA ---

# Directory where the satellite image dataset is stored
root_dir = 'map_tiles/combined_dataset'
# Check if CUDA (GPU) is available, otherwise use CPU
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# Custom dataset class for handling EV charger images
class EVChargerDataset(Dataset):
    def __init__(self, root_dir, transform=None):
        # Initialize the dataset with the root directory and transformations
        self.root_dir = root_dir
        self.transform = transform
        # Create list of all image files in the directory
        self.image_files = [f for f in os.listdir(root_dir) if os.path.isfile(os.path.join(root_dir, f))]

    def __len__(self):
        # Return the total number of images
        return len(self.image_files)

    def __getitem__(self, idx):
        # Get the image path and load the image with 3 channels (RGB)
        img_name = self.image_files[idx]
        img_path = os.path.join(self.root_dir, img_name)
        image = Image.open(img_path).convert('RGB')
        # Extract the label from the first character of the file name (1 for EV charger, 0 for no EV charger)
        label = int(img_name[0])
        if self.transform:
            image = self.transform(image)
        return image, label


# Define transformations for the images
transform = transforms.Compose([
    transforms.Resize((224, 224)),              # resize all images to 224x224
    transforms.RandomHorizontalFlip(p=0.3),     # apply horizontal flip with 30% probability
    transforms.RandomVerticalFlip(p=0.3),       # apply vertical flip with 30% probability
    transforms.RandomRotation(10),              # apply random rotation of up to 10 degrees
    transforms.ToTensor(),                      # convert the image to a PyTorch tensor
])

# Load the dataset and split it into training, validation, and test sets
dataset = EVChargerDataset(root_dir=root_dir, transform=transform)
# Split dataset into training (70%), validation (15%), and test (15%) sets
train_size = int(0.7 * len(dataset))            # 70% training data
val_size = int(0.15 * len(dataset))             # 15% validation data
test_size = len(dataset) - train_size - val_size    # 15% test data
train_dataset, val_dataset, test_dataset = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])

# Create data loaders for batching
batch_size = 32                                 # define batch size for loading data
train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)  # shuffle the training data
val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)     # validation data does not need shuffling
test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)   # test data does not need shuffling
print(f"Train dataset: {len(train_dataset)}\n"
      f"Val dataset: {len(val_dataset)}\n"
      f"Test dataset: {len(test_dataset)}")


# --- CREATING AND SELECTING MODELS ---

# Custom CNN architecture definition
class CustomCNN(nn.Module):
    def __init__(self):
        super(CustomCNN, self).__init__()
        # Define convolutional layers, fully connected layers, and dropout
        self.conv1 = nn.Conv2d(3, 16, 3, 1)
        self.conv2 = nn.Conv2d(16, 32, 3, 1)
        self.fc1 = nn.Linear(32*54*54, 128)     # image size affects number of parameters of model and first argument
        self.fc2 = nn.Linear(128, 2)            # output layer for 2 classes
        self.dropout = nn.Dropout(p=0.1)        # 10% dropout for regularisation
        self.name = 'Custom_CNN'

    def forward(self, x):
        # Define forward pass through the network
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2)
        x = torch.flatten(x, 1)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)                     # apply dropout
        x = self.fc2(x)
        return x


# MobileNetV3 architecture definition with fine-tuning
class FineTuneMobileNetV3(nn.Module):
    def __init__(self):
        super(FineTuneMobileNetV3, self).__init__()
        # Load pre-trained MobileNetV3 model and modify classifier for 2 output classes
        self.mobilenet = models.mobilenet_v3_large(pretrained=True)
        self.mobilenet.classifier[3] = nn.Linear(self.mobilenet.classifier[3].in_features, 2)
        self.name = 'MobileNetV3'

    def forward(self, x):
        return self.mobilenet(x)


# ResNet18 architecture definition with fine-tuning
class FineTuneResNet(nn.Module):
    def __init__(self):
        super(FineTuneResNet, self).__init__()
        # Load pre-trained ResNet18 model and modify the final fully connected layer for 2 output classes
        self.resnet = models.resnet18(pretrained=True)
        self.resnet.fc = nn.Linear(self.resnet.fc.in_features, 2)
        self.name = 'ResNet18'

    def forward(self, x):
        return self.resnet(x)


# VGG19 architecture definition with fine-tuning
class FineTuneVGG19(nn.Module):
    def __init__(self):
        super(FineTuneVGG19, self).__init__()
        # Load pre-trained VGG19 model and modify the final fully connected layer for 2 classes
        self.vgg = models.vgg19(pretrained=True)
        self.vgg.classifier[6] = nn.Linear(self.vgg.classifier[6].in_features, 2)
        self.name = 'VGG19'

    def forward(self, x):
        return self.vgg(x)


# Select the model (uncomment the desired model) and send it to the device (CPU or GPU)
model = CustomCNN()
# model = FineTuneMobileNetV3()
# model = FineTuneResNet()
# model = FineTuneVGG19()
model = model.to(device)                        # send model to GPU if available


# --- TRAINING ---

# Define hyperparameters and training utilities
learning_rate = 0.0001                          # learning rate for the optimiser
criterion = nn.CrossEntropyLoss()               # loss function (cross-entropy)
optimizer = torch.optim.Adam(model.parameters(),
                             lr=learning_rate, weight_decay=0.0001)     # Adam optimiser with weight decay
# Learning rate scheduler to reduce learning rate on plateau (minimizes validation loss)
scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.35, patience=5, verbose=True)

# Start timing the training process
start_time = time.time()

# Training loop
n_epochs = 16                                   # number of training epochs
train_losses = []
val_losses = []
for epoch in range(n_epochs):
    # Training phase
    model.train()                               # set model to training mode
    running_loss = 0.0
    for images, labels in train_loader:
        images, labels = images.to(device), labels.to(device)           # move data to GPU
        optimizer.zero_grad()                   # zero the gradients
        outputs = model(images)                 # forward pass
        loss = criterion(outputs, labels)       # compute loss
        loss.backward()                         # backpropagation
        optimizer.step()                        # update model parameters
        running_loss += loss.item()             # accumulate loss
    avg_train_loss = running_loss / len(train_loader)                   # average training loss
    train_losses.append(avg_train_loss)

    # Validation phase
    model.eval()                                # set model to evaluation mode
    val_loss = 0.0
    all_labels = []
    all_probs = []
    with torch.no_grad():                       # no gradient calculation in evaluation
        for images, labels in val_loader:
            images, labels = images.to(device), labels.to(device)       # move data to GPU
            outputs = model(images)             # forward pass
            probs = F.softmax(outputs, dim=1)[:, 1]                     # get probabilities for class 1 (EV charger)
            all_labels.extend(labels.cpu().numpy())
            all_probs.extend(probs.cpu().numpy())
            loss = criterion(outputs, labels)   # compute loss
            val_loss += loss.item()             # accumulate validation loss
    avg_val_loss = val_loss / len(val_loader)   # average validation loss across batches
    val_losses.append(avg_val_loss)

    # Calculate AUC-ROC score for validation set
    auc = roc_auc_score(all_labels, all_probs)

    # Print epoch details and calculate total training time
    end_time = time.time()
    training_time = end_time - start_time
    print(f"Epoch {epoch + 1}, Training Loss: {avg_train_loss:.3f}, Validation Loss: {avg_val_loss:.3f}, "
          f"AUC-ROC: {auc:.3f} | Total Training Time: {training_time:.2f} seconds.")

    # Adjust the learning rate based on validation loss, using the scheduler
    scheduler.step(avg_val_loss)

    # Plot training and validation loss
    plt.clf()                                   # clear the current figure for fresh plotting
    epochs = range(1, len(train_losses) + 1)
    plt.plot(epochs, train_losses, label='Training Loss', color='blue')
    plt.plot(epochs, val_losses, label='Validation Loss', color='orange')
    plt.xticks(epochs, fontsize=8)
    plt.yticks(fontsize=8)
    plt.xlim(1, n_epochs)
    plt.ylim(0, max(max(train_losses), max(val_losses)) + 0.1)
    plt.title(f'{model.name} - Training and Validation Loss over Epochs', pad=12, fontsize=12)
    plt.xlabel('Epoch', fontsize=10)
    plt.ylabel('Loss', fontsize=10)
    plt.legend(fontsize=9)
    plt.pause(0.1)                              # pause to update the figure with new data

# Save the final plot of training and validation loss
plt.savefig(f'{model.name}_train_val_loss.png', dpi=300)                # save the plot in the current directory

# Display total training time after completing all epochs
end_time = time.time()
training_time = end_time - start_time
print(f"Total training time: {training_time:.2f} seconds")

# Save the trained model's state (weights) to a file
torch.save(model.state_dict(), "trained_model.pth")
print("Model saved as trained_model.pth")


# --- TESTING ---

# Evaluate the model on the test dataset
model.eval()                                    # set model to evaluation mode
all_labels = []
all_probs = []

with torch.no_grad():                           # disable gradient computation
    for images, labels in test_loader:
        images, labels = images.to(device), labels.to(device)           # move data to GPU
        outputs = model(images)                 # forward pass through the model

        # Get the probabilities for the positive class (class 1: EV charger)
        probs = F.softmax(outputs, dim=1)[:, 1]
        all_labels.extend(labels.cpu().numpy())
        all_probs.extend(probs.cpu().numpy())

# Calculate the AUC-ROC score for the test set
test_auc = roc_auc_score(all_labels, all_probs)
print(f"Test AUC-ROC: {test_auc:.3f}")

# Compute false positive rate (FPR) and true positive rate (TPR) for ROC curve
fpr, tpr, thresholds = roc_curve(all_labels, all_probs)

# Plot the ROC curve for the test set
plt.figure()
plt.plot(fpr, tpr, color='blue', label=f'AUC-ROC = {test_auc:.3f}')
plt.plot([0, 1], [0, 1], color='red', linestyle='--')                   # random guessing line for reference
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title(f'{model.name} - ROC Curve', pad=12)
plt.legend(loc="lower right")

# Save the ROC curve plot to a file
plt.savefig(f'{model.name}_roc_curve.png', dpi=300)

# Calculate accuracy on the test set
model.eval()                                    # always make sure model is in evaluation mode for testing
correct = 0
total = 0
with torch.no_grad():                           # disable gradient computation
    for images, labels in test_loader:
        images, labels = images.to(device), labels.to(device)           # move data to GPU
        outputs = model(images)                 # forward pass
        _, predicted = torch.max(outputs.data, 1)                       # get the predicted class
        total += labels.size(0)                 # total number of test samples
        correct += (predicted == labels).sum().item()                   # count correct predictions

# Print the accuracy of the model on the test set
print(f"Accuracy (test set): {(100 * correct / total):.2f}%")

# Generate a classification report (precision, recall, f1-score) for the test set
model.eval()                                    # always make sure model is in evaluation mode for testing
all_labels = []                                 # predicted labels
all_preds = []                                  # true labels

with torch.no_grad():                           # disable gradient computation
    for images, labels in test_loader:
        images, labels = images.to(device), labels.to(device)           # move data to GPU
        outputs = model(images)                 # forward pass
        _, predicted = torch.max(outputs.data, 1)                       # get the predicted labels
        all_labels.extend(labels.cpu().numpy())
        all_preds.extend(predicted.cpu().numpy())

# Print detailed classification report. Includes precision, recall, and F1-Score for each class
print(classification_report(all_labels, all_preds))
