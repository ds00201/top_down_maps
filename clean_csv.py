import pandas as pd

# Load the CSV file.
file_path = 'national-charge-point-registry.csv'
df = pd.read_csv(file_path)

# Select only the required columns.
df_cleaned = df[['chargeDeviceID', 'latitude', 'longitude']]

# Save the cleaned dataframe to a new CSV file.
cleaned_file_path = 'cleaned_charge_point_registry.csv'
df_cleaned.to_csv(cleaned_file_path, index=False)

print(f"Cleaned file saved to: {cleaned_file_path}")
