import os
import shutil
import random


# --- Splitting combined dataset images into train and val in structure for YOLO ---

# Define the base directory and various paths
base_dir = os.path.abspath(os.path.dirname(__file__))       # get the absolute path of the current directory
combined_dataset_dir = os.path.join(base_dir, 'map_tiles_12k', 'combined_dataset')  # contains combined dataset images
output_dir = os.path.join(base_dir, 'map_tiles', 'images')  # directory to store the split datasets
train_dir = os.path.join(output_dir, 'train')               # directory for training images
val_dir = os.path.join(output_dir, 'val')                   # directory for validation images

# Create train and val directories if they don't already exist
os.makedirs(train_dir, exist_ok=True)
os.makedirs(val_dir, exist_ok=True)

# Get a list of all the .png image files in the combined_dataset directory
png_files = [f for f in os.listdir(combined_dataset_dir) if f.endswith('.png')]

# Shuffle the list of image files to randomise the dataset
random.shuffle(png_files)

# Split the shuffled list into 80% training and 20% validation sets
split_index = int(len(png_files) * 0.8)                     # calculate the index for the 80/20 split
train_files = png_files[:split_index]                       # 80% of the files go into the training set
val_files = png_files[split_index:]                         # 20% of the files go into the validation set

# Copy the training files to the train directory
for file in train_files:
    src = os.path.join(combined_dataset_dir, file)          # source path for the file
    dst = os.path.join(train_dir, file)                     # destination path for the file
    shutil.copyfile(src, dst)                               # copy the file to the train directory

# Copy the validation files to the val directory
for file in val_files:
    src = os.path.join(combined_dataset_dir, file)          # source path for the file
    dst = os.path.join(val_dir, file)                       # destination path for the file
    shutil.copyfile(src, dst)                               # copy the file to the validation directory

# Print out how many files were copied to each directory
print(f"Copied {len(train_files)} files to {train_dir}")
print(f"Copied {len(val_files)} files to {val_dir}")


# --- Creating associated label files for YOLO format ---

# Define directories for images and labels
base_dir = os.path.abspath(os.path.dirname(__file__))
images_dir = os.path.join(base_dir, 'map_tiles', 'images')  # path to the images directory
labels_dir = os.path.join(base_dir, 'map_tiles', 'labels')  # path to the labels directory
train_images_dir = os.path.join(images_dir, 'train')        # path to the train images directory
val_images_dir = os.path.join(images_dir, 'val')            # path to the validation images directory
train_labels_dir = os.path.join(labels_dir, 'train')        # path to the train labels directory
val_labels_dir = os.path.join(labels_dir, 'val')            # path to the validation labels directory

# Create the label directories for train and val sets if they do not already exist
os.makedirs(train_labels_dir, exist_ok=True)
os.makedirs(val_labels_dir, exist_ok=True)


# Function to create label files for YOLO
def create_label_files(image_dir, label_dir):
    # Iterate through each image file in the image directory
    for image_file in os.listdir(image_dir):
        if image_file.endswith('.png'):                     # only process .png files
            # Determine the label based on the file name: if it starts with '1', it represents a positive label
            if image_file.startswith('1'):
                # YOLO format: "class, class x_center, y_center, width, height". Where 0 is the class for EV charger
                # Note that 'no EV charger' does not have a class for YOLO format
                label_content = "0 0.5 0.5 0.35 0.35"       # class 0 (EV charger) with bounding box parameters
            else:
                # If the file doesn't start with '1', create an empty label file (no EV charger)
                label_content = ""

            # Generate the corresponding label file name with .txt extension
            label_file = os.path.splitext(image_file)[0] + '.txt'
            label_path = os.path.join(label_dir, label_file)            # full path to save the label file

            # Write the label content to the label file
            with open(label_path, 'w') as f:
                f.write(label_content)


# Create label files for the training images
create_label_files(train_images_dir, train_labels_dir)
# Create label files for the validation images
create_label_files(val_images_dir, val_labels_dir)

# Print out a confirmation message
print(f"Label files created in {train_labels_dir} and {val_labels_dir}")
