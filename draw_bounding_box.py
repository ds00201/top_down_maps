import cv2
import matplotlib.pyplot as plt
import os


# Function to draw a bounding box on an image based on a YOLO label file
def draw_bounding_box(image_path, label_path):
    # Check if the image file exists
    if not os.path.exists(image_path):
        print(f"Error: Image file '{image_path}' not found.")
        return

    # Check if the label file exists
    if not os.path.exists(label_path):
        print(f"Error: Label file '{label_path}' not found.")
        return

    # Load the image using OpenCV
    image = cv2.imread(image_path)
    # Convert the image from BGR (OpenCV format) to RGB (standard format for visualization)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # Load the label file (assumed to have only one bounding box per image)
    with open(label_path, 'r') as file:
        label_content = file.readline().strip()     # read the first line and remove any extra spaces

    # If the label is empty, print a message and display the image without a bounding box
    if not label_content:
        print(f"No bounding box for '{image_path}'. Displaying image without bounding box.")
        plt.figure(figsize=(10, 10))                # set the figure size for display
        plt.imshow(image)                           # display the image
        plt.axis('off')                             # turn of the axis for a cleaner display
        plt.show()                                  # show the image
        return

    # Process the label content if it is not empty
    label = label_content.split()                   # split the label content by spaces
    print(f"Label content: {label}")

    # Check if the label contains exactly 5 elements (YOLO format: class_id, x_center, y_center, width, height)
    if len(label) != 5:
        print(f"Error: Label file '{label_path}' is incorrectly formatted.")
        return

    # Extract the bounding box information from the label (all values are in YOLO format)
    class_id, x_center, y_center, width, height = map(float, label)         # convert strings to floats

    # Convert YOLO format (relative values) to actual pixel coordinates
    img_height, img_width, _ = image.shape          # get the image dimensions (height and width)
    x_center *= img_width                           # scale the x_center by the image width
    y_center *= img_height                          # scale the y_center by the image height
    width *= img_width                              # scale the width by the image width
    height *= img_height                            # scale the height by the image height

    # Calculate the top-left (x1, y1) and bottom-right (x2, y2) coordinates of the bounding box
    x1 = int(x_center - width / 2)
    y1 = int(y_center - height / 2)
    x2 = int(x_center + width / 2)
    y2 = int(y_center + height / 2)

    # Draw the bounding box on the image using OpenCV's rectangle function
    cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0), 2)                # blue box with thickness of 2

    # Display the image with the bounding box using matplotlib
    plt.figure(figsize=(10, 10))                    # set the figure size for display
    plt.imshow(image)                               # show the image
    plt.axis('off')                                 # turn off the axis for a cleaner display
    plt.show()                                      # show the image with the bounding box


# --- EXAMPLE USAGE ---

# Define the paths to the image and label files, your path may be different
image_path = 'map_tiles/images/train/1_map_tile_52.05057_-2.71775.png'
label_path = 'map_tiles/labels/train/1_map_tile_52.05057_-2.71775.txt'

# Call the function to draw the bounding box on the image
draw_bounding_box(image_path, label_path)
